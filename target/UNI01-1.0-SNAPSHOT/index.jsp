<%-- 
    Document   : index
    Created on : 01-04-2020, 20:22:10
    Author     : Lewiss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="estilos.css">
        <title>Calculadora</title>
    </head>
    <body>  
        <div class="formcontainer">
            <h1>CALCULADORA INTERESES SIMPLES</h1>
                 <form name="form" action="controller" method="POST">
                     <label> Capital</label>
                     <input type="number" name="capital" placeholder="$" step="any" value="" required/>
                     <label> Tasa </label>
                     <input type="number" name="tasa" placeholder="%" step="any" value="" required/>
                     <label> Años </label>
                     <input type="number" name="ano" placeholder="#" step="any" value="" required/>
                     <input type="submit" value="=" />    
                </form>
        </div>
    </body>
    
</html>
