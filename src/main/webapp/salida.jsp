<%-- 
    Document   : salida
    Created on : 01-04-2020, 20:28:21
    Author     : Lewiss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" type="text/css" href="estilos.css">
        <title>Calculadora</title>
    </head>
    <body>
        
        <%
            float capital = (float) request.getAttribute("cap");
            float tasa = (float) request.getAttribute("tas");
            float ano = (float) request.getAttribute("an");
            float interes = (float) request.getAttribute("interes");
        %>
        <div class="tablecontainer">
            <table border="1">
                <thead>
                    <tr>
                        <th align="center" colspan="2">CALCULADORA INTERESES SIMPLES</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Capital</td>
                        <td><%=capital%></td>
                    </tr>
                      <tr>
                        <td>Tasa</td>
                        <td><%=tasa%></td>
                    </tr>
                      <tr>
                        <td>Años</td>
                        <td><%=ano%></td>
                    </tr>
                      <tr>
                        <td>INTERESES</td>
                        <td><%=interes%></td>
                    </tr>
                </tbody>
            </table>
         <a href="http://localhost:8080/UNI01/" target="_parent"><<<<<</a>   
        </div>
       
    </body>
</html>
